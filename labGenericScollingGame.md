# Generic Scrolling Game: I dont know yet

#Objectives

Throughout this exercise the students will practice:

* if/else
* do/while
* setter/getters

## Concepts

This laboratory consist on a simple side scrolling game. The user controls a car on the left edge of the screen, that can be moved up and down. Obstacles and checkpoints are created on the right edge of the screen, and these scroll toward the user on the left edge. The user earns a score, shown at the bottom, and the game ends when the user hits an obstacle. This laboratory is an adaptation of the assigment presented by Dave Feinberg [1].


## Libraries
For this project you need to know how to use the rand function 
   
* srand(time(NULL)); // to initialize random number function
* rand( );           // random number function

## Exercise 1

The Qt project at *HERE???* contains the skeleton for an application to ... The application allows the user to ....

<img src="http://i.imgur.com/cH1Cxbn.png">

Specific instructions for the exercise.

## Exercise 2

## Optional Exercise 


### Deliverables

In the following text box, copy the functions that you developed for the program. Remember to properly comment all functions and use good indentation and variable naming practices.

### References
[1] Dave Feinberg, http://nifty.stanford.edu/2011/feinberg-generic-scrolling-game/

