#include "checkpoint.h"

checkpoint::checkpoint(QWidget *parent) :
    QWidget(parent)
{
    x_flag = 250;
    y_flag = 140;
    name = ":/images/flag.png";
}

void checkpoint::setCheckpoint(string arg){

}

string checkpoint::getCheckpoint(){
    return name;
}

void checkpoint::setXflag(int arg){
    x_flag = arg;
}

void checkpoint::setYflag(int arg){
    y_flag = arg;
}

int checkpoint::getXflag(){
    return x_flag;
}

int checkpoint::getYflag(){
    return y_flag;
}
