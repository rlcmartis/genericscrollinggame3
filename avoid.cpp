#include "avoid.h"

avoid::avoid(QWidget *parent) :
    QWidget(parent)
{
    x_cone=200;
    y_cone=250;
}

void avoid::setObs(string arg){
    if(arg == "cone"){
        name = ":/images/cone.png";
    }
    else if (arg == "hole") {
        name = ":/images/hoyo.png";
    }
    else if(arg == "monster"){
        name = ":/images/monster.png";
    }
    else if(arg == "zombie"){
        name = ":/images/zombie.png";
    }
    else if(arg == "it"){
        name = ":/images/it.png";
    }
    else if(arg == "spongebob"){
        name = ":/images/spongebob.png";
    }
    else if(arg == "patrick"){
        name = ":/images/patrick.png";
    }
}

string avoid::getObs(){
    return name;
}

int avoid::getXcone(){
    return x_cone;
}

int avoid::getYcone(){
    return y_cone;
}

void avoid::setXcone(int arg){
    x_cone = arg;
}

void avoid::setYcone(int arg){
    y_cone = arg;
}
